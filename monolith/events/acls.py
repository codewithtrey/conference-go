from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    # Use the Pexels API

    # url = 'https://api.pexels.com/v1/search'
    # headers = {'Authorization': 'PEXELS_API_KEY'}

    response = requests.get(f"https://api.pexels.com/v1/search?query={city}+{state}",
                            headers={
                                "Authorization": PEXELS_API_KEY
                            })

    data = json.loads(response.text)

    return data["photos"][0]["src"]["original"]
    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response


def get_weather_data(city, state):
    # Use the Open Weather API
    # OPEN_WEATHER_API_KEY
    geo_response = requests.get(f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}&units=imperial")

    data = geo_response.json()
    # print(data)
    lat = data[0]["lat"]
    lon = data[0]["lon"]
    # weather = {
    #     "lat": data["lat"],
    #     "lon": data["lon"],
    # }
    # Create the URL for the geocoding API with the city and state
    # http://api.openweathermap.org/geo/1.0/direct?q={city name},{state code},{country code}&limit={limit}&appid={API key}
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response


    response = requests.get(f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial",
                            headers={
                                "Authorization": OPEN_WEATHER_API_KEY
                            })

    data = response.json()

    new_data = {
        "temperature": data["main"]["temp"],
        "weather": data["weather"][0]["description"],
    }

    return new_data
    # Create the URL for the current weather API with the latitude
    # https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
